# Time utilities #

Datastructures and helper functions to deal with timestamped data.
In particular the Timeline class allows one to quickly query and aggregate time series data. 


## Example ##
```java
public class TimelineDemo {

    /**
     * Your custom POJO with a point in time
     */
    public static class Event implements Timestamped {
        DateTime time;
        String name;

        public Event(DateTime time, String name) {
            this.time = time;
            this.name = name;
        }

        @Override
        public DateTime getTimeStamp() {
            return time;
        }
    }

    public static void main(String[] args) {

        // Create a timeline
        System.out.println("Init");
        Timeline<Event> timeLine = new Timeline<>();

        // Add events (in any order)
        timeLine.add(new Event(new DateTime(2016, 2, 1, 12, 30), "a1"));
        timeLine.add(new Event(new DateTime(2016, 1, 1, 12, 30),"a2"));
        timeLine.add(new Event(new DateTime(2016, 3, 1, 12, 30),"a3"));
        timeLine.add(new Event(new DateTime(2016, 4, 1, 12, 30),"a4"));
        timeLine.add(new Event(new DateTime(2016, 3, 1, 12, 30),"a5"));

        // Show all events
        System.out.println("test iterator");
        for (Event event : timeLine) {
            System.out.println(event);
        }

        timeLine.print();

        // Number of elements stored
        System.out.println("timeLine.count() = " + timeLine.count());

        // Slice of the timeline
        System.out.println("Select");
        Timeline select = timeLine.select(new DateTime(2016, 1, 15, 12, 30), new DateTime(2016, 3, 15, 12, 30));
        select.print();
        System.out.println("select.count() = " + select.count());


        System.out.println("Filter");
        timeLine.filter(e -> e.name.contains("2")).print();
        
    }
}

```

## Credits ##
Arvid Halma (HumanityX, Centre for Innovation, Leiden University)

## License ##
Copyright 2018 Centre for Innovation, Leiden University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.