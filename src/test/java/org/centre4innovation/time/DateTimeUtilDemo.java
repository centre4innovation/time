package org.centre4innovation.time;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * @author Arvid Halma
 * @version 28-11-19
 */
public class DateTimeUtilDemo {
    public static void main(String[] args) {

        DateTimeZone.setDefault(DateTimeZone.forID("Africa/Bangui"));
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm:ss");
        System.out.println(dateTimeFormatter.parseDateTime("29-03-2015 2:25:15"));

        dateTimeFormatter = DateTimeFormat.forPattern("HH:mm:ss");
        System.out.println(dateTimeFormatter.parseDateTime("2:25:15"));

        final DateTime t = new DateTime(2019, 11, 28, 17, 19);
        System.out.println("t = " + t);
        System.out.println("round H = " + DateTimeUtil.roundBottom(t, Period.hours(1)));
        System.out.println("round d = " + DateTimeUtil.roundBottom(t, Period.days(1)));
        System.out.println("round m = " + DateTimeUtil.roundBottom(t, Period.months(1)));
        System.out.println("round y = " + DateTimeUtil.roundBottom(t, Period.years(1)));
    }
}
