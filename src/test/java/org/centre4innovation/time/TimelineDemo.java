package org.centre4innovation.time;

import org.joda.time.DateTime;
import org.joda.time.Period;

/**
 * Demonstration of how timelines can be used.
 * @version 30-1-17
 * @author Arvid Halma
 */
public class TimelineDemo {

    static class Event {
        String name;
        double spent;

        public Event(String name, double spent) {
            this.name = name;
            this.spent = spent;
        }

        public String getName() {
            return name;
        }

        public double getSpent() {
            return spent;
        }

        @Override
        public String toString() {
            return "{" + name + ", " + spent + '}';
        }
    }

    public static void main(String[] args) {

        // Create a timeline
        Timeline<Event> timeLine = new Timeline<>();

        // Add events (in any order)
        timeLine.add(new DateTime(2016, 1, 1, 12, 30), new Event("e1", 2.10));
        timeLine.add(new DateTime(2016, 4, 1, 12, 30),new Event("e10", 6.00));
        timeLine.add(new DateTime(2016, 2, 1, 12, 30), new Event("e2", 1.40));
        timeLine.add(new DateTime(2016, 2, 15, 12, 30), new Event("e4", 5.1));
        timeLine.add(new DateTime(2016, 3, 1, 12, 30),new Event("e5", 13.7));
        timeLine.add(new DateTime(2016, 3, 5, 12, 30),new Event("e7", 7.6));
        timeLine.add(new DateTime(2016, 2, 2, 12, 30), new Event("e3", 3.41));
        timeLine.add(new DateTime(2016, 3, 5, 12, 30),new Event("e8", 2.80));
        timeLine.add(new DateTime(2016, 3, 5, 12, 30),new Event("e9", 1.90));
        timeLine.add(new DateTime(2016, 3, 1, 12, 30),new Event("e6", 2.0));

        // Show all events
        System.out.println("\nPrint");
        timeLine.print();

        // Slice of the timeline
        System.out.println("\nSelect");
        Timeline<Event> select = timeLine.select(new DateTime(2016, 2, 10, 0, 0), new DateTime(2016, 3, 7, 0, 0));
        select.print();

        System.out.println("\nSelect (autoTrim)");
        Timeline<Event> selectAutoTrim = timeLine.select(new DateTime(2016, 2, 10, 0, 0), new DateTime(2016, 3, 7, 0, 0), true);
        selectAutoTrim.print();

        System.out.println("\nSelect (trim)");
        Timeline<Event> selectTrim = timeLine.select(new DateTime(2016, 2, 10, 0, 0), new DateTime(2016, 3, 7, 0, 0));
        selectTrim.trim();
        selectTrim.print();


        System.out.println("select.count() = " + select.count());


        System.out.println("\nFilter");
        timeLine.filter(e -> e.spent > 3).print();

        System.out.println("\nGet (existing)");
        System.out.println(timeLine.get(new DateTime(2016, 3, 5, 12, 30)));

        System.out.println("\nGet (non-existing)");
        System.out.println(timeLine.get(new DateTime(2016, 3, 17, 12, 30)));

        System.out.println("\nGet Floor (non-existing)");
        System.out.println(timeLine.getFloorEntry(new DateTime(2016, 3, 17, 12, 30)));

        System.out.println("\nGet Ceil (non-existing)");
        System.out.println(timeLine.getCeilEntry(new DateTime(2016, 3, 17, 12, 30)));

        System.out.println("\nInterpolated average (existing)");
        System.out.println(timeLine.interpolatedValue(new DateTime(2016, 3, 5, 12, 30), Event::getSpent, StatReduce.MEAN));

        System.out.println("\nInterpolated average (non-existing)");
        System.out.println(timeLine.interpolatedValue(new DateTime(2016, 3, 17, 12, 30), Event::getSpent, StatReduce.MEAN));

        System.out.println("\nResampled average series");
        final Timeline<Double> resampled = select.resample(Period.days(1), Event::getSpent, StatReduce.MEAN, 0.0);
        resampled.print();

        System.out.println("\nResampled average series trimmed");
        final Timeline<Double> resampled2 = selectTrim.resample(Period.days(1), Event::getSpent, StatReduce.MEAN, 0.0);
        resampled2.print();

        System.out.println("\nInterpolated average series");
        final Timeline<Double> interpolated = select.interpolate(Period.days(1), Event::getSpent, StatReduce.MEAN);
        interpolated.print();

        System.out.println("\nInterpolated average series trimmed");
        final Timeline<Double> interpolated2 = selectTrim.interpolate(Period.days(1), Event::getSpent, StatReduce.MEAN);
        interpolated2.print();





    }
}
